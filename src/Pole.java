import java.util.Stack;

public class Pole {	
	private Stack<Disk> disks;
	private String name;
	
	//Initialiser
	public Pole(String name) {
		disks = new Stack<Disk>();
		this.name = name;
		
		//If disk is 'A', add starter disks to pole.
		if(name == "A") {
			for(int i = 3; i >= 1; i--) {
				this.disks.add(new Disk(i));
			}
		}				
	}
	
	//Getters for instance
	public String getName() {
		return this.name.toUpperCase();		
	}	
	
	///Functions		
	public void printDisks() {
		for(int i = 0; i < this.disks.size(); i++) {
			TextIO.putln("The " + this.disks.get(i).getSize() + " disk is on pole " + this.getName());
		}
	}	
	
	public void addDisk(Disk disk) {		
		if(this.disks.size() > 0 && disk.getSizeInt() > this.peepDisk().getSizeInt()) {
			throw new IllegalArgumentException("Can't add disk to pole!");
		} else {
			this.disks.add(disk);
		}
	}	
	
	public Disk peepDisk() {
		return this.disks.size() > 0 ? this.disks.peek() : null;
	}
	
	public Disk popDisk() {
		return this.disks.pop();
	}	
	
	public Boolean hasWon() {
		return this.disks.size() == 3 && this.getName() == "C";
	}
}

import java.util.ArrayList;
import java.util.List;

public class Hanoi {
	
	public static List<Pole> poles;

	public static void main(String[] args) {
		outputHelpText();
		
		//Initialise poles
		poles = new ArrayList<Pole>();		
		poles.add(new Pole("A"));
		poles.add(new Pole("B"));
		poles.add(new Pole("C"));	
		
		//Loop-da-loop
		loop();		
	}
		
	private static void loop() {
		while(!poles.get(2).hasWon()) {			
			printDisks();				
				
			//Get disk selection
			TextIO.putln("Which disk would you like to move (S=Small, M=Medium, L=Large):");
			String diskSelection = TextIO.getlnString().trim().toUpperCase();
			switch(diskSelection) {
			case "S":				
			case "M":
			case "L":				
				break;
			default: 
				invalidSelection();
			}
					
			//Get pole selection
			TextIO.putln("Which pole do you want to move the disk to (A=Pole A, B=Pole B, C=Pole C): ");
			Pole pole = null;			
			switch(TextIO.getlnString().trim().toUpperCase()) {
			case "A":
				pole = poles.get(0);
				break;
			case "B":
				pole = poles.get(1);
				break;
			case "C":
				pole = poles.get(2);
				break;
			default: 
				invalidSelection();				
			}
								
			//Loop through poles and see if disk is available for movement...
			Boolean moved = false;
			for(int i = 0; i < poles.size(); i++) {
				Pole p = poles.get(i);
				//Dont scan pole if pole is target.
				if(!p.equals(pole)) {
					Disk peepDisk = p.peepDisk();												
					if(!moved && peepDisk != null && p.peepDisk().getName().equals(diskSelection)) {
						try {
							pole.addDisk(p.peepDisk());
							p.popDisk();
							moved = true;
						} catch(IllegalArgumentException ex) {
						}
					}
				} 
			}
			
			if(!moved) {
				TextIO.putln("Invalid move!");
				TextIO.putln("");				
			}			
		}
		
		TextIO.putln("");
		TextIO.putln("You've completed the game! Congratulations!");
	}
		
	private static void printDisks() {
		for(int i = 0; i < poles.size(); i++) {
			poles.get(i).printDisks();
		}
	}
		
	private static void invalidSelection() {
		TextIO.putln("");
		TextIO.putln("Invalid selection..");
		loop();				
	}
	
	private static void outputHelpText() {
		TextIO.putln("Welcome to the puzzle of the mini-towers of Hanoi!");
		TextIO.putln("In this version of the puzzle there are three disks and three poles.");
		TextIO.putln("There is a small disk, a medium disk and a large disk.");
		TextIO.putln("Each disk has a hole in the middle which allows it to be placed on a pole.");
		TextIO.putln("The three poles are arranged left to right and labelled A, B and C.");
		TextIO.putln("Initially, all three disks are on the left-most pole, A.");
		TextIO.putln("Your task is to move the three disks to the right-most pole, C.");
		TextIO.putln("The problem is that you can only move one disk at a time and");
		TextIO.putln("you cannot put a larger disk on top of a smaller disk.");
		TextIO.putln("You can use pole B as a temporary pole for one or more disks");
		TextIO.putln("but to solve the problem all three disks must ultimately end up");
		TextIO.putln("on the right-most pole, C.");		
		TextIO.putln();
	}
}


public class Disk {
	private int size;
	
	//Initialiser
	public Disk(int size) {
		this.size = size;
	}
	
	public String getName() {
		switch(this.size) {
		case 1: return("S");
		case 2: return("M");
		case 3: return("L");
		default: return("mysterious");
		}
	}
		
	public String getSize() {
		switch(this.size) {
			case 1: return("small");
			case 2: return("medium");
			case 3: return("large");
			default: return("mysterious");
		}
	}
	
	public int getSizeInt() {
		return this.size;
	}
}
